// Objetos para iniciar el server y hacer peticiones HTTP
var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var requestJSON = require("request-json");
var mongoLibrary = require('mongodb');
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// Objetos donde se cargan las funciones API cuentas, movimientos, usuarios y clientes
var usuarios_api = require("./api-js/usuarios-api");
var cuentas_api = require("./api-js/cuentas-api");
var movimientos_api = require("./api-js/movimientos-api");
var clientes_api = require("./api-js/clientes-api");
var comercios_api = require("./api-js/comercios-api");
var servicios_api = require("./api-js/servicios-api");
var db = require("./api-js/mongo-connection");


//Conectamos a la BBDD e iniciamos API
db.driverBD(mongoLibrary,
    function(err, db){

        mongoLibrary.db = db;
        
        usuarios_api.usuarios(app,requestJSON);
        cuentas_api.cuentasCliente(app,mongoLibrary);
        movimientos_api.movimientosCuentas(app,mongoLibrary);
        clientes_api.clientes(app,requestJSON);
        comercios_api.comercios(app,requestJSON);
        servicios_api.servicios(app,requestJSON);
        app.listen(4000);

        console.log("Escuchando en el puerto 4000");

    }
);