
module.exports = {
    
    comercios: function(app, requestJSON){

    
        // Método para consultar todos los comercios

        app.get('/v1/comercios',function(request, response){


            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/comercios?apiKey=' + apiKey;
            
            var clienteMLab = requestJSON.createClient(uriMLab);
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
            });
            
        
        });
        

         // Método para consultar un comercio en especifico dado el idComercio

         app.get('/v1/comercios/:idComercio',function(request, response){

            var idComercio = request.params.idComercio;


            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/comercios?q={"_id":{"$oid":"' + idComercio +'"}}&apiKey=' + apiKey;
            console.log(uriMLab)
            var clienteMLab = requestJSON.createClient(uriMLab);
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
        
        
            });
            
        
        });


        // Método para dar de alta un comercio

        app.post('/v1/comercios',function(request, response){
        
        
            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/comercios?&apiKey=' + apiKey;
            
            
            var clienteMLab = requestJSON.createClient(uriMLab);
            var nuevoComercio = request.body;
        
            clienteMLab.post('', nuevoComercio,
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    
                    response.status(500);
                    response.send(err);
                }
        
        
            });

          
        });
        
        
         // Método para modificar una comercio dado el idComercio

         app.put('/v1/comercios', function(request,response){

           
        
        });
      

    }
}
