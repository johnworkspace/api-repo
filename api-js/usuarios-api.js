
module.exports = {
    
    usuarios: function(app, requestJSON){

         // Método para consultar todos los usuarios
         
         app.get('/v1/usuarios',function(request, response){

            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/usuarios?f={"password":0, "estatusConexion":0}&apiKey=' + apiKey;
            var clienteMLab = requestJSON.createClient(uriMLab );
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
        
        
            });
            
        
        });
        



         // Método para consultar un usuario en especifico dado el mail

         app.get('/v1/usuarios/:email',function(request, response){

            var email = request.params.email;
            var idCliente = request.query.idCliente;

            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab= 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes?f={"password":0, "estatusConexion":0,"cuentas.movimientos":0,"cuentas.limiteCredito":0,"cuentas.saldo":0}&q={"email":"' + email +'"}&apiKey=' + apiKey;
            
            console.log(uriMLab)
            
            var clienteMLab = requestJSON.createClient(uriMLab);
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
        
        
            });
            
        
        });

        //Metodo para hacer login

        app.post('/v1/usuarios/login',function(request, response){

            var userMail = request.body.email;
            var userPass = request.body.password;
            console.log("Cuerpo peticion " + request.body);
            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes?f={"password":0, "estatusConexion":0}&q={"email":"' + userMail +'"}&apiKey=' + apiKey;
            console.log(uriMLab);
            var clienteMLab = requestJSON.createClient(uriMLab);
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    console.log(body);
                    if (body.length == 0){

                        response.status(409);
                        response.send({"code":"NONEXISTENT_USER"}); 


                    }else{

                        var cliente = body[0];

                        if(cliente.usuario.activo){

                            if(cliente.usuario.password == userPass){

                                response.send({"code":"OK","idCliente":cliente._id.$oid}); 

                            }else{
                                    response.status(409);
                                    response.send({"code":"INVALID_PASSWORD"}); 
                            }

                        }else{

                            response.status(409);
                            response.send({"code":"INACTIVE_USER"});

                        }
                    }
        
                }else{
                    response.status(500);
                    response.send(err);
                }
            });
        });


        // Método para dar de alta un usuario dado el mail

        app.post('/v1/usuarios/alta',function(request, response){
        
            
            var email = request.body.email;
            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab ='https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes?f={"password":0, "estatusConexion":0,"cuentas.movimientos":0,"cuentas.limiteCredito":0,"cuentas.saldo":0}&q={"email":"' + email +'"}&apiKey=' + apiKey;
            var clienteMLab = requestJSON.createClient(uriMLab);
            var nuevoUsuario = request.body;
        
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    //console.log("respuesta mlab: " + JSON.stringify(body));
                    

                    if(body.length == 0){

                        response.status(409);
                        response.send({"code":"NONEXISTENT_USER"}); 
                        return;

                    }else{

                            var cliente = body[0];

                            if(cliente.usuario.activo){

                                response.status(409);
                                response.send({"code":"ALREADY_ACTIVE_USER"}); 
                                return;

                            }else{

                                var success = validarCuentas(request.body,cliente);
                                console.log("success: "+ success);
                                if(success){

                                    altaUsuarioMongo(nuevoUsuario,requestJSON,function(err,respuesta){

                                        if(!err){

                                            console.log(respuesta);
                                            response.send({"code":"OK"}); 
                                            
                                        }else{
                                            response.status(500);
                                            response.send(err); 
                                            return;
                                        }

                                    });

                                }else{

                                    response.status(409);
                                    response.send({"code":"ACCOUNT_NIP_DONT_MATCH"}); 
                                    return;

                                }

                            } 
                    }
        
                }else{
                    response.status(500);
                    response.send(err);
                }
            });


        });
        
        
         // Método para modificar un usuario dado el idCliente

         app.put('/v1/usuarios/:idUsuario', function(request,response){

            var idUsuario = request.params.idUsuario;

            var uriMlab = "https://api.mlab.com/api/1/databases/techumxjhsa/collections/";
            var apiKey =  "apiKey=Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var clienteMLab = requestJSON.createClient(uriMlab + "usuarios/" );
            var comentario = "";

            if (request.body._id != undefined ){

                delete usuarioModificado.idUsuario;
                comentario = "No se puede modificar ID, se procede a actualizar sin modificar ID";
            }

            var requestBody = JSON.parse('{ "$set":' + JSON.stringify(request.body) + '}');

            console.log('?q={"_id":{$oid:"'+ idUsuario +'"}}'+ ((request.query.m!=undefined)?'&m='+request.query.m:"") + '&' +apiKey);
            clienteMLab.put('?q={"_id":{$oid:"'+ idUsuario +'"}}' + ((request.query.m!=undefined)?'&m='+request.query.m:"") + '&' +apiKey,requestBody,
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    body.comments  = comentario;
                    response.send(body);
        
                }else{
                    response.send(err);
                }      
            });       
        });
    }
}


function validarCuentas(apiData, clienteMongo){

    //console.log("Mongo: "+JSON.stringify(clienteMongo));
    var idCuenta = apiData.idCuenta;
    var nip = apiData.nip;

    //console.log("El nip es: " + nip +" "+ idCuenta);

    var cuentas = clienteMongo.cuentas;
    var cta;
    

    for (var i in cuentas) {

        cta = cuentas[i];
        
        if((cta.numTarjeta == idCuenta)&&(cta.nip == nip) ){
            
            console.log("Verdadero");
            return true;

        }
    }
    return false;
}

    
function altaUsuarioMongo(bodyMlab, requestJSON,callback){

    var uriMlab = "https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes";
    var apiKey =  "apiKey=Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
    var clienteMLab = requestJSON.createClient(uriMlab);

    var query = {"email":bodyMlab.email};
    var peticionMlab = {$set:{"usuario.activo":true, "usuario.password":bodyMlab.password}};

    clienteMLab.put('?q='+ JSON.stringify(query) + '&' +apiKey,peticionMlab,
    
    function(err,resMLab,body){

            callback(err,body);
                        
    });       
} 