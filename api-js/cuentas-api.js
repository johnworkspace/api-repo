module.exports = {
    
    cuentasCliente: function(app, mongoLibrary){

        // Método para consultar las cuentas de un cliente dado el idCliente o consultar una cuenta en especifico dado el numCuenta (tarjeta)

        app.get('/v1/cuentas',function(request, response){
                
            var clientes = mongoLibrary.db.collection("clientes");
            var idCliente = request.query.idCliente;
            var idCuenta = request.query.idCuenta;
            var tipoTarjeta = request.query.tipoTarjeta;

            if (idCliente == null || idCliente == undefined){

                var err = {"message":"Error, no se puede consultar sin idCliente"}
                
                response.status(500);
                response.send(err);


            }else{

                var o_id = new mongoLibrary.ObjectID(idCliente);  

                
   
                if (idCuenta == null || idCuenta == undefined){
        

                    if(tipoTarjeta != null || tipoTarjeta!= undefined){

                        clientes.aggregate([
                            { "$match": { "_id": o_id} }, {"$project":{
                                                "cuentas":{
                                                  
                                                              "$filter":{
                                                                           "input": {"$map": {  "input": "$cuentas", "as":"cuenta", "in": {"numTarjeta":"$$cuenta.numTarjeta", "saldo": "$$cuenta.saldo", "saldoActual":"$$cuenta.saldoActual","limiteCredito":"$$cuenta.limiteCredito", "tipoTarjeta":"$$cuenta.tipoTarjeta"} } },
                                                                           "as": "cta",
                                                                           "cond": {$eq: ["$$cta.tipoTarjeta",tipoTarjeta] } 
                                                              }	  
                                                }
                                            }}]).toArray(function(err,docs){
            
                                                if(err) {response.status(500); response.send(err);}
                                                response.send(docs);
                                                
                                            
                                            });
                    }else{

                            clientes.aggregate([
                                { "$match": { "_id": o_id }}, {"$project":{
                                                    "cuentas":{
                                                                "$map": {  "input": "$cuentas", "as":"cuenta", "in": {"numTarjeta":"$$cuenta.numTarjeta", "saldoActual":"$$cuenta.saldoActual","saldo": "$$cuenta.saldo", "limiteCredito":"$$cuenta.limiteCredito", "tipoTarjeta":"$$cuenta.tipoTarjeta"}  }
                                                    }
                                                }}]).toArray(function(err,docs){
                
                                                    if(err) {response.status(500); response.send(err);}
                                                    
                                                    response.send(docs);                     
                                                
                                                });
                    }                    

                }else{

                    clientes.aggregate([
                        { "$match": { "_id": o_id} }, {"$project":{
                                            "cuentas":{
                                              
                                                          "$filter":{
                                                                       "input": {"$map": {  "input": "$cuentas", "as":"cuenta", "in": {"numTarjeta":"$$cuenta.numTarjeta", "saldo": "$$cuenta.saldo", "saldoActual":"$$cuenta.saldoActual","limiteCredito":"$$cuenta.limiteCredito", "tipoTarjeta":"$$cuenta.tipoTarjeta"} } },
                                                                       "as": "cta",
                                                                       "cond": {$eq: ["$$cta.numTarjeta",idCuenta] } 
                                                          }	  
                                            }
                                        }}]).toArray(function(err,docs){
        
                                            if(err) {response.status(500); response.send(err);}
                                            response.send(docs);
                                            
                                        
                                        });

                }
                
            }
      
        });
        
              
         // Método para modificar el NIP de una tarjeta  dado el idCliente e idCuenta

         app.put('/v1/cuentas', function(request,response){

            var clientes = mongoLibrary.db.collection("clientes");
            var idCuenta = request.body.idCuenta;
            var idCliente = request.body.idCliente;
            var nip = request.body.nip;
            console.log(nip);

            if(idCuenta == undefined || idCliente == undefined){
                response.status(409);
                response.send ({"message":"Es necesario ingresar el idCuenta y idCliente"});
            }

            var o_id = new mongoLibrary.ObjectID(idCliente);  

            var query = {"_id": o_id, "cuentas.numTarjeta":idCuenta};
            var updateValues = { $set:  {"cuentas.$.nip": nip } };
            
            clientes.updateOne(query, updateValues, function(err, res) {
                if (err){response.status(500);response.send(err)};
                response.send(res);
              });
      
        });
      
    }
}