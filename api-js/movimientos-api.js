
module.exports = {
    
    movimientosCuentas: function(app, mongoLibrary){

          // Método para consultar todos los movimientos dado un idCliente e idCuenta + un movimiento en especifico dado el idMovimiento(opcional)

          app.get('/v1/movimientos',function(request, response){

            var clientes = mongoLibrary.db.collection("clientes");
            var idCuenta = request.query.idCuenta;
            var idCliente = request.query.idCliente;
            var idMovimiento = request.query.idMovimiento;

            if(idCuenta == undefined || idCliente == undefined){
                response.status(409);
                response.send ({"message":"Es necesario ingresar el idCuenta y idCliente"});
            }

            var o_id = new mongoLibrary.ObjectID(idCliente);  


            if(!idMovimiento){ 
        
                clientes.aggregate([
                    { "$match": { "_id": o_id} }, {"$project":{
                                        "cuentas":{
                                          
                                                      "$filter":{
                                                                   "input": {"$map": {  "input": "$cuentas", "as":"cuenta", "in": {"numTarjeta":"$$cuenta.numTarjeta", "saldo": "$$cuenta.saldo", "limiteCredito":"$$cuenta.limiteCredito", "tipoTarjeta":"$$cuenta.tipoTarjeta","movimientos":"$$cuenta.movimientos"} } },
                                                                   "as": "cta",
                                                                   "cond": {$eq: ["$$cta.numTarjeta",idCuenta] } 
                                                      }	  
                                        }
                                    }}]).toArray(function(err,docs){
        
                                            if(err) {response.status(500); response.send(err);}
                                            response.send(docs);
                                            
                                        
                                        });

            }else{
                
                var m_id = new mongoLibrary.ObjectID(idMovimiento);

                clientes.aggregate([
                    { "$match": { "_id": o_id} }  ,{ "$project": {
                                              
                                       "cuentas": {
                                      "$filter": {
                                                    "input": {
                                                              "$map": {
                                                                        "input": "$cuentas",
                                                                        "as": "cta",
                                                                        "in": {
                                                                               "numTarjeta": "$$cta.numTarjeta",
                                                                               "movimientos": {
                                                                                                 "$filter": {
                                                                                                             "input": "$$cta.movimientos",
                                                                                                                 "as": "mov",
                                                                                                             "cond": { $eq: [ "$$mov.idMovimiento", m_id]}
                                                                                                   }
                                                                                   }
                                                                         }
                                                                }
                                                    },
                                                    "as": "cta",
                                                "cond": { $eq: ["$$cta.numTarjeta" ,idCuenta] }
                                                  }	
                                    }
                                         
                              }}
                    ]).toArray(function(err,docs){
        
                                            if(err) {response.status(500); response.send(err);}
                                            response.send(docs);
                                            
                                        
                                        });

            }          
        
        });
        



        // Método para dar de alta un movimiento dado el idCliente e idCuenta

        app.post('/v1/movimientos',function(request, response){
        
        
            var clientes = mongoLibrary.db.collection("clientes");
            var idCuenta = request.body.idCuenta;
            var idCliente = request.body.idCliente;
            var tipoTarjeta = request.body.tipoTarjeta;

            delete request.body.idCuenta;
            delete request.body.idCliente;
            delete request.body.tipoTarjeta;

            if(idCuenta == undefined || idCliente == undefined){
                response.status(409);
                response.send ({"message":"Es necesario ingresar el idCuenta y idCliente"});
                return;
            }

            if(tipoTarjeta ==undefined){
                response.status(409);
                response.send ({"message":"Es necesario ingresar el tipo de tarjeta"});
                return;
            }

            var date = new Date();
            var hora = date.getHours();
            var minuto = date.getMinutes();
            var segundo = date.getSeconds();
            var dia = date.getUTCDay();
            var mes = date.getMonth();
            var anio = date.getFullYear();
            var fecha = dia+'/'+mes+'/'+anio+' ' + hora+':'+minuto+':'+segundo;
       
            var o_id = new mongoLibrary.ObjectID(idCliente);
            var idMovimiento = new mongoLibrary.ObjectID();  
            
            request.body.idMovimiento = idMovimiento;
            request.body.fechaMovimiento = fecha;

            var query = {"_id": o_id, "cuentas.numTarjeta":idCuenta};
            console.log(query);
            
            var updateValues;

            if(tipoTarjeta=='C'){

                updateValues = { "$push": {"cuentas.$.movimientos": request.body },"$inc":{"cuentas.$.saldoActual": request.body.importe, "cuentas.$.saldo":-request.body.importe }};
                console.log(updateValues);

            }else{

                updateValues = { "$push": {"cuentas.$.movimientos": request.body },"$inc":{"cuentas.$.saldoActual": -request.body.importe }};
                console.log(updateValues);

            }

            clientes.updateOne(query, updateValues, function(err, res) {
                if (err){console.log("hubo un error!!!");response.status(500);response.send(err); return};
                response.send(res);
              });
           
        });
        
        


         // Método para pagar una TDC

        app.post('/v1/movimientos/pagoTDC',function(request, response){

            var clientes = mongoLibrary.db.collection("clientes");
            var idCuentaTDC = request.body.idCuenta;
            var idCliente = request.body.idCliente;
            var cuentaDebito = request.body.tarjetaPago;
            var idCuentaTDD = cuentaDebito.numTarjeta;
            var montoMovimiento = request.body.importe;

            delete request.body.idCuenta;
            delete request.body.idCliente;
            delete request.body.tarjetaPago


            if(idCuentaTDD == undefined || idCuentaTDC == undefined || idCliente == undefined){
                response.status(409);
                response.send ({"message":"Es necesario ingresar el idCuenta y idCliente"});
                return;
            }

            var date = new Date();
            var hora = date.getHours();
            var minuto = date.getMinutes();
            var segundo = date.getSeconds();
            var dia = date.getUTCDay();
            var mes = date.getMonth();
            var anio = date.getFullYear();
            var fecha = dia+'/'+mes+'/'+anio+' ' + hora+':'+minuto+':'+segundo;
    

            var o_id = new mongoLibrary.ObjectID(idCliente);
            var idMovimientoTDC = new mongoLibrary.ObjectID();  
            var idMovimientoTDD = new mongoLibrary.ObjectID();  
            
            var jsonTDD = request.body;
            var jsonTDC = request.body;

            jsonTDC.idMovimiento = idMovimientoTDC;
            jsonTDD.idMovimiento = idMovimientoTDD;
            jsonTDC.fechaMovimiento = jsonTDD.fechaMovimiento = fecha;

            var queryTDC= {"_id": o_id, "cuentas.numTarjeta":idCuentaTDC};
            var queryTDD= {"_id": o_id, "cuentas.numTarjeta":idCuentaTDD};

            console.log("JSON TDC: "+ JSON.stringify(queryTDC));
            console.log("JSON TDD: "+ JSON.stringify(queryTDD));

            var updateValuesTDC = { "$push": {"cuentas.$.movimientos": jsonTDC },"$inc":{"cuentas.$.saldoActual": -jsonTDC.importe, "cuentas.$.saldo":jsonTDC.importe }};
            var updateValuesTDD = { "$push": {"cuentas.$.movimientos": jsonTDD },"$inc":{"cuentas.$.saldoActual": -jsonTDD.importe }};


            clientes.updateOne(queryTDD, updateValuesTDD, function(err, res) {
                if (err){console.log("hubo un error!!!");response.status(500);response.send(err); return};

                clientes.updateOne(queryTDC, updateValuesTDC, function(err, res) {
                    if (err){console.log("hubo un error!!!");response.status(500);response.send(err); return};
                    response.send(res);
                  });
              });
        });
       
    }
}
