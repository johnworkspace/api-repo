
module.exports = {
    
    clientes: function(app, requestJSON){

         // Método para consultar todos los clientes

        app.get('/v1/clientes',function(request, response){


            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes?f={"cuentas":0}&apiKey=' + apiKey;
            
            var clienteMLab = requestJSON.createClient(uriMLab);
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
            });
        });
        
        

         // Método para consultar un cliente en especifico dado el idCliente

         app.get('/v1/clientes/:idCliente',function(request, response){


            var idCliente = request.params.idCliente;

            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes?f={"cuentas":0}&q={"_id":{"$oid":"' + idCliente +'"}}&apiKey=' + apiKey;
            console.log(uriMLab)
            var clienteMLab = requestJSON.createClient(uriMLab);
            
            clienteMLab.get('',
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
        
        
            });
            
        
        });


        // Método para dar de alta un cliente

        app.post('/v1/clientes',function(request, response){
        

            var apiKey = "Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var uriMLab = 'https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes?&apiKey=' + apiKey;
            
            
            var clienteMLab = requestJSON.createClient(uriMLab);
            var nuevoUsuario = request.body;
            nuevoUsuario.estado = false;
        
        
            clienteMLab.post('', nuevoUsuario,
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    response.send(body);
        
                }else{
                    response.send(err);
                }
        
        
            });
        });
        
        
         // Método para modificar un cliente dado el idCliente

         app.put('/v1/clientes/:idCliente', function(request,response){

            var idCliente = request.params.idCliente;

            var uriMlab = "https://api.mlab.com/api/1/databases/techumxjhsa/collections/clientes/";
            var apiKey =  "apiKey=Lg9xrzueE2nGrgAcmxnBhvFPwIV8MUae";
            var clienteMLab = requestJSON.createClient(uriMlab);
        
            console.log(idCliente);

            var clienteModificado = request.body;
            var comentario = undefined;

            if (clienteModificado._id != undefined){

                delete clienteModificado._id;
                comentario = "No se puede modificar ID, se procede a actualizar sin modificar ID";
            }


            var requestBody = JSON.parse('{ "$set":' + JSON.stringify(request.body) + '}');
            
            console.log('?q={"_id":{"$oid":' + idCliente + '}}' + ((request.query.m!=undefined)?'&m='+request.query.m:"") + '&' +apiKey);
            clienteMLab.put('?q={"_id":{"$oid":"' + idCliente + '"}}' + ((request.query.m!=undefined)?'&m='+request.query.m:"") + '&' +apiKey,requestBody,
            
            function(err,resMLab,body){
        
                if(!err){
                    
                    if (comentario!= undefined){

                        body.comment = comentario;
                    }

                    response.send(body);
        
                }else{
                    response.send(err);
                }
        
        
            });
        
        });
       

    }
}
